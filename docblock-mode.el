;;; docblock-mode.el --- Provide a unified interface for documentation block generation  -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Sebastian Fieber

;; Author: Sebastian Fieber <sebastian.fieber@web.de>
;; Keywords: convenience, languages

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'cl-lib)
(require 'subr-x)
(require 'thunk)
(require 'xref)

(defgroup docblock '()
  "Customization group of `docblock-mode'"
  :group 'tools
  :prefix "docblock-")

(defcustom docblock-use-yasnippet (if (require 'yasnippet nil t)
                                      t
                                    nil)
  "Defines if yasnippet is used in generated docblocks."
  :type 'boolean
  :safe t
  :group 'docblock)

(defvar docblock-command-map
  (let ((map (make-sparse-keymap)))
    (define-key map "i" #'docblock-insert)
    (define-key map "e" #'docblock-edit-doc-for-symbol-at-point)
    map)
  "Command map for docblock used with prefix \"C-c d\".")

(defvar docblock-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-c d") docblock-command-map)
    map)
  "Keymap for `docblock-mode'.

\\{docblock-mode-map}")


(defvar docblock-special-major-mode-list '()
  "Register a custom implementation for a specific major mode.

The format should be an alist whose entries car represents the major
mode and the cdr is the entry function for the implementation.  The
entry function must process the symbols EDIT or INSERT.  See
`docblock-insert' and `docblock-edit-doc-for-symbol-at-point'.")

(defvar docblock-enabled-modes-list '((c-mode . docblock-cc-init)
                                      (c++-mode . docblock-cc-init)
                                      (php-mode . docblock-php-init)
                                      ;; (perl-mode . docblock-perl-init)
                                      ;; (cperl-mode . docblock-perl-init)
                                      ;; (java-mode . docblock-java-init)
                                      ;; (python-mode . docblock-python-init)
                                      ;; (js-mode . docblock-js-init)
                                      ;; (js2-mode . docblock-js-init)
                                      ;; (ruby-mode . docblock-ruby-init)
                                      )
  "Alist of modes for which docblock functionalities are defined.

Every entry musut have the major mode in its cdr and the init
function in its cdr.")

(defvar docblock--current-thing-cache nil)
(defvar docblock--yas-no-abort-or-leave nil)

(defvar-local docblock-function-regexp nil
  "Regexp which should match a function for current major mode.

Regexp search will be called right before
`docblock-parse-function-function' is called with match strings
intact.

The regexp MUST match with `re-search-forward' as well as with
`search-backward-regexp'.")

(defvar-local docblock-function-decl-regexp nil
  "Regexp which should match a function declaration (e.g. no
body) for current major mode.

Regexp search will be called right before
`docblock-parse-function-function' is called with match strings
intact.")

(defvar-local docblock-class-regexp nil
  "Regexp which should match a class for current major mode.

Regexp search will be called right before
`docblock-parse-class-function' is called with match strings
intact.  Class maybe some other equivalent structure in used
major mode.

The regexp MUST match with `re-search-forward' as well as with
`search-backward-regexp'.")

(defvar-local docblock-variable-regexp nil
  "Regexp which should match a variable declaration.

Regexp search will be called right before
`docblock-parse-variable-function' is called with match-strings
intact.

The regexp MUST match with `re-search-forward' as well as with
`search-backward-regexp'.")

(defvar-local docblock-tag-regexp " \@[a-z]+"
  "Regexp to identify a tag in a docblock.")

(defvar-local docblock-prepare-tag-line-regexp (lambda (regexp) regexp)
  "This function is called for each line when merging docblocks.

This gives a specifc implementation the chance to alter the
regular expression which is used to check if lines differ in the
existing and the newly generated docblock.  Initially the regexp
will be set to match the new line.")

(defvar-local docblock-doc-comment-start "/**"
  "Defines how the first line of a docblock is represented.")

(defvar-local docblock-doc-comment-continue " * "
  "Defines how each line of a docblock should start.")

(defvar-local docblock-doc-comment-end " */"
  "Defines how the last line of a docblock is represented.")

(defvar-local docblock-parse-function-function nil
  "The function which is called when a function is found to document.

Right before the function a regexp search with the regexp defined
in `docblock-function-regexp' is performed so match-strings are
intact when this function is called.  The function's return value
will be `apply'ed to `docblock--generate-doxygen-like-doc-string'
and must match its parameters.")

(defvar-local docblock-parse-class-function nil
  "The function which is called when a class is found to document.

Right before the function a regexp search with the regexp defined
in `docblock-class-regexp' is performed so match-strings are
intact when this function is called.  The function's return value
will be `apply'ed to `docblock--generate-doxygen-like-doc-string'
and must match its parameters.")

(defvar-local docblock-parse-variable-function nil
  "The function which is called when a class is found to document.

Right before the function a regexp search with the regexp defined
in `docblock-variable-regexp' is performed so match-strings are
intact when this function is called.  The functions return value
will be `apply'ed to `docblock--generate-doxygen-like-doc-string'
and must match its parameters.")

(defvar-local docblock-beginning-of-statement (lambda ()
                                                (save-match-data
                                                  (let ((call
                                                         (save-excursion
                                                           (eq (c-beginning-of-statement-1) 'same))))
                                                    (when call
                                                      (c-beginning-of-statement-1)))))
  "Change this to something apropriate if not in a c derived major mode.")

(defvar-local docblock-tag-whitelist '()
  "List of tags which should be ignored by an update.

All tags inside this list will not be touched by an update.")


(defun docblock--register-new-sepcial-major-mode (mmode fun)
  "Register a new implementation of docblock features for MMODE.

MMODE stands for the `major-mode' and FUN for the entry function."
  (cl-pushnew `(,mmode . ,fun) docblock-special-major-mode-list))

(defmacro docblock--if-major-mode-implementation-existing (sentry-fun &rest body)
  "Call SENTRY-FUN or execute BODY if docbklock is defined.

SENTRY-FUN is called if current `major-mode' is car of a member
of `docblock-special-major-mode-list'.  BODY is executed if it is
member of `docblock-enabled-modes-list'.  Signals an error if
docblock functionality is not defined for the current buffers
`major-mode'.

The variables entry and sentry are set to the cons cells where
car is matching current `major-mode' either from
`docblock-enabled-modes-list' (entry) or from
`docblock-special-major-mode-list' (sentry)."
  (declare (indent defun))
  `(let ((entry (assoc major-mode docblock-enabled-modes-list))
         (sentry (assoc major-mode docblock-special-major-mode-list)))
     (if sentry
         ,sentry-fun
       (if entry
           (progn ,@body)
         (error
          (format "No docblock funtionality defined for \"%s\""
                  major-mode)
          (docblock-mode -1))))))

(defun docblock-insert ()
  "Insert a docblock for thing at point.

Thing can be a function, class or (member) variable.  If there is
already a docblock it will be updated."
  (interactive)
  (docblock--if-major-mode-implementation-existing
    (funcall-interactively (cdr sentry) 'insert)
    (docblock--insert-internal)))

(defun docblock-edit-doc-for-symbol-at-point ()
  "Edit the documentation for symbol at point.

If there is no documentation block one is created and point will
be set to the position where the documentation for symbol should
be placed."
  (interactive)
  (docblock--if-major-mode-implementation-existing
    (funcall-interactively (cdr sentry) 'edit)
    (docblock--edit-internal)))

;; helper functions

(defun docblock--get-point-after-call (fn)
  "Go to end of line, call FN and return current position."
  (save-excursion
    (end-of-line)
    (funcall fn)
    (point)))

(defun docblock--get-function-bounds ()
  "Get the current functions start and end point.

Internally `beginning-of-defun' and `end-of-defun' will be called
so make sure that point is inside a function.  Return value is a
cons cell."
  (save-excursion
    (or (save-match-data
          (funcall docblock-beginning-of-statement)
          (when  (looking-at docblock-function-decl-regexp)
            `(,(match-beginning 0) . ,(match-end 0))))
        (condition-case nil
            `(,(progn (end-of-line) (beginning-of-defun) (point)) .
              ,(progn (beginning-of-line) (end-of-defun) (backward-char) (point)))
          (error nil)))))

(defun docblock--pos-inside-function (point function-pos)
  "Check if POINT is inside the function starting at FUNCTION-POS."
  (save-excursion
    (goto-char function-pos)
    (let* ((bounds (docblock--get-function-bounds))
           (start (car bounds))
           (end (cdr bounds)))
      (and (>= point start)
           (<= point end)))))

(defun docblock--get-thing-to-document ()
  "Get the position and the type of the thing to document.

Thing can be a class, function or variable.  Returned is a cons
cell with a symbol representing the type as its car value and the
start position of that as its cdr.  `docblock-function-regexp',
`docblock-variable-regexp' and `docblock-class-regexp' are used for
retrieving the start position of thing."
  (if (null docblock--current-thing-cache)
      (cl-flet ((search-fn ()
                           (when (not (null docblock-function-regexp))
                             (save-excursion
                               (or
                                (progn
                                  (funcall docblock-beginning-of-statement)
                                  (beginning-of-line)
                                  (if (looking-at docblock-function-decl-regexp)
                                      (point)
                                    nil))
                                (condition-case nil
                                    (or
                                     ;; if point is at the start of
                                     ;; the function decleration
                                     ;; narrow-to-defun and
                                     ;; beginning-of-defun may
                                     ;; "misbehave" so we check this first
                                     (when (looking-at docblock-function-regexp)
                                       (point))
                                     (save-restriction
                                       (narrow-to-defun)
                                       (beginning-of-defun)
                                       (when (looking-at docblock-function-regexp)
                                         (point)))))))))
                (search-class ()
                              (when (not (null docblock-class-regexp))
                                (save-excursion
                                  (end-of-line)
                                  (search-backward-regexp docblock-class-regexp
                                                          nil
                                                          t))))
                (search-var ()
                            (when (not (null docblock-variable-regexp))
                              (save-excursion
                                (end-of-line)
                                (search-backward-regexp docblock-variable-regexp
                                                        nil
                                                        t)))))
        (let* ((p (point))
               (nearest-fn (thunk-delay (search-fn)))
               (nearest-class (thunk-delay (search-class)))
               (nearest-var (thunk-delay (search-var)))
               (use-fn (thunk-delay (when (not (null (thunk-force nearest-fn)))
                                      (save-match-data
                                        (docblock--pos-inside-function p (thunk-force nearest-fn))))))
               (use-var (thunk-delay (when (not (null (thunk-force nearest-var)))
                                       (save-excursion
                                         (funcall docblock-beginning-of-statement)
                                         (looking-at-p docblock-variable-regexp)))))
               (use-class (thunk-delay (not (null (thunk-force nearest-class))))))
          ;; make match-string stuff right
          (setq docblock--current-thing-cache
                (cond ((thunk-force use-fn) `(fn . ,(thunk-force nearest-fn)))
                      ((thunk-force use-var) `(var . ,(thunk-force nearest-var)))
                      ((thunk-force use-class) `(class . ,(thunk-force nearest-class)))))))
    docblock--current-thing-cache))

(defun docblock--generate-doxygen-like-doc-string (space tags)
  "Generate a doxygen like doc string.

SPACE is a string and should only contain whitespace.  It is
preceeded to any line added to the documentation.  TAGS should be
an alist with the tag string as car and a boolean value as cdr
which controls if a yasnippet marker is placed.  Of course if
`docblock-use-yasnippet' the cdr will be ignored."
  (let* ((start (concat space docblock-doc-comment-start "\n"
                        space docblock-doc-comment-continue
                        (if docblock-use-yasnippet
                            "${}\n"
                          "\n")
                        space docblock-doc-comment-continue "\n"))
         (end   (concat space docblock-doc-comment-end "\n"))
         (tag-string (cl-reduce (lambda (string param)
                                  (concat string
                                          space
                                          docblock-doc-comment-continue
                                          (car param)
                                          (if (and docblock-use-yasnippet
                                                   (cdr param) )
                                              " ${}\n"
                                            "\n")))
                                tags
                                :initial-value "")))
    (concat start
            tag-string
            end)))

(defun docblock--goto-beginning-of-thing (pos thing)
  "Go to POS, call `beginning-of-line' and return THING."
  (goto-char pos)
  (funcall docblock-beginning-of-statement)
  thing)

(defun docblock--goto-beginning-of-current-thing ()
  "Go to beginning of current thing.

Thing maybe class, function or variable."
  (let* ((thing-and-pos (docblock--get-thing-to-document)))
    (when (not (null thing-and-pos))
      (docblock--goto-beginning-of-thing (cdr thing-and-pos)
                                         (car thing-and-pos)))))



(defun docblock--thing-at-point-has-docblock ()
  ""
  (save-excursion
    (docblock--goto-beginning-of-current-thing)
    (let ((regexp (concat "^[\s\t]*"
                          (regexp-quote
                           (string-trim docblock-doc-comment-start))
                          ".*\n"
                          "\\("
                          "[\s\t]*"
                          (regexp-quote
                           (string-trim docblock-doc-comment-continue))
                          ".*\n"
                          "\\)*"
                          "[\s\t]*"
                          (regexp-quote
                           (string-trim docblock-doc-comment-end))
                          "[\s\t]*"
                          "\n")))
      (if (looking-back regexp nil)
          (substring-no-properties (match-string 0))
        nil))))

(defun docblock--insert-internal ()
  "Internal function to insert a doxygen like docblock.

If there already exists a docblock for the currrent thing to
document.  It will be tried to update."
  ;; fix for bad jump caused by deleting a newline when calling this
  ;; and point is at beginning of the line of the start of thing
  (setq docblock--current-thing-cache nil)
  (forward-char)
  (xref-push-marker-stack)
  (backward-char)
  (cl-flet ((generate-doc-block
             (yas-p)
             (let ((docblock-use-yasnippet yas-p)
                   (type (docblock--goto-beginning-of-current-thing)))
               (cond ((eq type 'fn)
                      (apply #'docblock--generate-doxygen-like-doc-string
                             (funcall docblock-parse-function-function)))
                     ((eq type 'class)
                      (apply #'docblock--generate-doxygen-like-doc-string
                             (funcall docblock-parse-class-function)))
                     ((eq type 'var)
                      (apply #'docblock--generate-doxygen-like-doc-string
                             (funcall docblock-parse-variable-function)))))))
    (let ((insert-fn (if docblock-use-yasnippet
                         (lambda (string x y hook)
                           (advice-add  'yas-exit-snippet :before
                                        #'docblock--yas-abort-advice)
                           (funcall 'yas-expand-snippet string x y hook))
                       ;; x and y are no used as we mimic the
                       ;; signature of yas-expand-snippet
                       (lambda (string x y hook)
                         (insert string)))))
      (funcall insert-fn
               (let ((new-doc (generate-doc-block nil))
                     (new-doc-res (generate-doc-block docblock-use-yasnippet))
                     (old-doc (docblock--thing-at-point-has-docblock))
                     (old-doc-beg (match-beginning 0)))
                 (if old-doc
                     (progn
                       (save-match-data
                         (search-backward old-doc old-doc-beg)
                         (replace-match "")
                         (docblock--merge-docblock-lines old-doc new-doc)))
                   new-doc-res))
               nil
               nil
               '((yas/after-exit-snippet-hook 'docblock--yas-after-exit-snippet-hook))))))

(defun docblock--yas-after-exit-snippet-hook ()
  (when docblock--yas-no-abort-or-leave
    (xref-pop-marker-stack)
    (backward-char))
  (advice-remove 'yas-exit-snippet
                 #'docblock--yas-abort-advice)
  (setq docblock--yas-no-abort-or-leave nil))

(defun docblock--yas-abort-advice (snippet)
  (setq docblock--yas-no-abort-or-leave t))

(defun docblock--edit-internal ()
  "Todo."
  (error "Not implemented yet"))


;;; merge stuff ;;;
(defun docblock--get-lines-belonging-to-tag (lines n &optional res)
  "Get the corresponding doc-lines which belong to a specific line.

LINES is a list of all lines of the raw existing docblock.  N is
the index of the line for which the related lines (like
documentation for the specific tag) should be returned.  Returned
is a list of lines.  RES should not be used and is used for
recursion purpose."
  (let ((line-after-tag (nth (+ n 1) lines))
        (res (or res
                 `(,(nth n lines)))))
    (if (and (stringp line-after-tag)
             (and (not (string-match-p
                        docblock-tag-regexp line-after-tag))
                  (not (string-match-p
                        (regexp-quote docblock-doc-comment-end) line-after-tag))))
        (docblock--get-lines-belonging-to-tag lines
                                              (+ n 1)
                                              (cons line-after-tag res))
      res)))

(defun docblock--merge-guess-reason (new-lines n old-lines m)
  "Guess why there is a difference between two lines.

NEW-LINES is a list containing all lines with a tag of the
freshly generated docblock and N is the index of the compared
line.  OLD-LINES contains all lines with a tag of the existing
docblock and M is the equivalent for N but for OLD-LINES.

Returned is one of the following symbols: swap, delete, insert,
rename."
  (let* ((new-line (nth n new-lines))
         (old-line (nth m old-lines))
         (new-line-in-old-lines-p (cl-some
                                   (lambda (line)
                                     (string-match-p
                                      ;; prepare regexp for new line
                                      ;; (implementation specific)
                                      (funcall docblock-prepare-tag-line-regexp
                                               (regexp-quote new-line))
                                      line))
                                   old-lines))
         (old-line-in-new-lines-p (cl-some
                                   (lambda (line)
                                     (string-match-p (regexp-quote old-line)
                                                     line))
                                   new-lines))
         (previous-old-line (nth (- m 1) old-lines))
         (previous-new-line (nth (- n 1) new-lines))
         (next-old-line (nth (+ m 1) old-lines))
         (next-new-line (nth (+ n 1) new-lines)))
    (cond ((and old-line-in-new-lines-p
                new-line-in-old-lines-p)
           'swap)
          ((and (not old-line-in-new-lines-p)
                new-line-in-old-lines-p)
           'delete)
          ((and old-line-in-new-lines-p
                (not new-line-in-old-lines-p))
           'insert)
          ((and (not old-line-in-new-lines-p)
                (not new-line-in-old-lines-p))
           ;; maybe rename or insert
           (if (or (= (length old-lines) (length new-lines))
                   (and (and (stringp previous-new-line)
                             (stringp previous-old-line)
                             (string-match (regexp-quote previous-new-line)
                                           previous-old-line))
                        (and (stringp next-new-line)
                             (stringp next-old-line)
                             (string-match (regexp-quote next-new-line)
                                           next-old-line))))
               'rename
             'insert)))))

(defun docblock--get-old-doc-string (lines line)
  "Get the old documentation string for specific line.

LINES is a list of all raw lines of the existing docblock.
LINE is the specific line for which all related lines should be
included in the returned string."
  (concat
   (string-join
    (reverse
     (docblock--get-lines-belonging-to-tag lines
                                           (seq-position
                                            lines
                                            line
                                            (lambda (cur-line line)
                                              (string-match-p (regexp-quote line)
                                                              cur-line)))))
    "\n")
   "\n"))

(defun docblock--merge-handle-swap (pure-old-lines
                                    new-line)
  "Handle a swap of two lines in existing and new docblock.

PURE-OLD-LINES is a list of all raw lines of the old docblock.
NEW-LINE is the line in the new docblock which is swapped."
  (let ((value (docblock--get-old-doc-string pure-old-lines new-line)))
    `((value  . ,value)
      (new-offset . 0)
      (old-offset . 0))))

(defun docblock--merge-handle-delete (pure-old-lines old-line)
  "Handle a deletion of a line in existing and new docblock.

PURE-OLD-LINES is a list of all raw lines of the old docblock.
OLD-LINE is the line in the existing docblock which is not
existing in the newly generated docblock.  The deleted lines are
saved to the kill ring.  `interprogram-cut-function' is temporary
set to nil to not mess up the desktop clipboard."
  (let ((interprogram-cut-function nil))
    (kill-new (docblock--get-old-doc-string pure-old-lines old-line)))
  '((value  . "")
    (new-offset . -1)
    (old-offset .  0)))

(defun docblock--merge-handle-rename (pure-old-lines
                                      new-line
                                      old-line)
  "Handle a change of a line in existing and new docblock.

PURE-OLD-LINES is a list of all raw lines of the old docblock.
NEW-LINE is the line in the new docblock which is
renamed.  OLD-LINE is the corresponding line in the old docblock which
is affected."
  (let* ((old-value (docblock--get-old-doc-string pure-old-lines old-line))
         (spaces (cl-reduce (lambda (a b)
                              (+ a (if (= b ? ) 1 0)))
                            (string-to-list new-line)
                            :initial-value 1))
         (old-doc-part (substring old-value
                                  ;; cut off space
                                  (+ 1 (let ((ispaces spaces)
                                             (list (string-to-list old-line))
                                             res)
                                         (dotimes (i (length list) res)
                                           (if (and (> ispaces 0)
                                                    (= (nth i list) ? ))
                                               (setq res i
                                                     ispaces (- ispaces 1))))))
                                  ;; cut off newline
                                  (- (length old-value) 1)))
         (new-value (if docblock-use-yasnippet
                        (concat new-line " ${" old-doc-part "}\n")
                      (concat new-line old-doc-part))))
    `((value  . ,new-value)
      (new-offset . 0)
      (old-offset . 0))))

(defun docblock--merge-handle-insert (new-line)
  "Handle an insert of a line in the new docblock.

NEW-LINE is the line to be inserted."
  `((value  . ,(if docblock-use-yasnippet
                   (concat new-line "${}\n")
                 (concat new-line "\n")))
    (new-offset .  0)
    (old-offset . -1)))

(defun docblock--merge-handle-reason (reason
                                      pure-old-lines
                                      old-line
                                      new-line)
  "Delegate to a function to handle REASON.

REASON is the cause of two lines to differ in the newly generated
and the old existing docblock.  PURE-OLD-LINES is a list of all
raw lines of the old docblock.  OLD-LINE is the line in the
existing docblock which differs from NEW-LINE in the newly
generated one."
  (cond ((eq reason 'swap) (docblock--merge-handle-swap pure-old-lines
                                                        new-line))
        ((eq reason 'delete) (docblock--merge-handle-delete pure-old-lines
                                                            old-line))
        ((eq reason 'rename) (docblock--merge-handle-rename pure-old-lines
                                                            new-line
                                                            old-line))
        ((eq reason 'insert) (docblock--merge-handle-insert new-line))
        (t '((value  . "")
             (new-offset . 0)
             (old-offset . 0)))))

(defun docblock--merge-recurse (i
                                j
                                new-lines
                                old-lines
                                pure-new-lines
                                pure-old-lines
                                &optional res)
  "Check each line of both docblocks and handle differences.

I is the current index of a line in the new-lines which is
compared to index J in old-lines.  NEW-LINES is alist consiting
of all lines with tags inside the newly generated docblock.
OLD-LINES is the equivalent to NEW-LINES but for the existing
docblock.  PURE-NEW-LINES is a list of all raw lines of the new
docblock.  PURE-OLD-LINES is the equivalent to PURE-NEW-LINES.
RES is an internal parameter used for recursion."
  (let ((new-line (nth i new-lines))
        (old-line (nth j old-lines))
        (res (or res "")))
    (cond

     ;; no more new lines?
     ((null new-line)
      res)

     ;; no more old lines?
     ((null old-line)
      (docblock--merge-recurse
       (+ i 1)
       j
       new-lines
       old-lines
       pure-new-lines
       pure-old-lines
       (concat res new-line "\n")))

     ;; if not in our white list don't touch
     ((not (seq-some (lambda (whitelisted-tag)
                       (string-match-p (regexp-quote whitelisted-tag) old-line))
                     docblock-tag-whitelist))
      (docblock--merge-recurse
       i
       (+ j 1)
       new-lines
       old-lines
       pure-new-lines
       pure-old-lines
       (concat res
               (docblock--get-old-doc-string pure-old-lines
                                             old-line))))
     ;; if lines match
     ((string-match-p
       (funcall docblock-prepare-tag-line-regexp
                (replace-regexp-in-string
                 " "
                 "[ \t]*"
                 (regexp-quote new-line)))
       old-line)
      (docblock--merge-recurse
       (+ i 1)
       (+ j 1)
       new-lines
       old-lines
       pure-new-lines
       pure-old-lines
       (concat res
               (docblock--get-old-doc-string pure-old-lines
                                             old-line))))
     ;; if not check for reason and handle it
     (t
      (let* ((merge-info (docblock--merge-handle-reason
                          (docblock--merge-guess-reason new-lines
                                                        i
                                                        old-lines
                                                        j)
                          pure-old-lines
                          old-line
                          new-line))
             (value (cdr (assoc 'value merge-info)))
             (new-offset (cdr (assoc 'new-offset merge-info)))
             (old-offset (cdr (assoc 'old-offset merge-info))))
        (docblock--merge-recurse (+ i 1 new-offset)
                                 (+ j 1 old-offset)
                                 new-lines
                                 old-lines
                                 pure-new-lines
                                 pure-old-lines
                                 (concat res value)))))))

(defun docblock--merge-get-doc-until-first-tag (lines &optional n res)
  "Get the documentation until the first tag from LINES.

LINES is a list of strings representing lines in a docblock.
Returned is a string representing the docblock right before the
first tag.  N and RES are both for internal recursion use only.
Where N is the current index in LINES and RES is the result
string."
  (let* ((n (or n 0))
         (res (or res ""))
         (current (nth n lines)))
    (if (or (string-match-p (regexp-quote docblock-doc-comment-end) current)
            (string-match-p docblock-tag-regexp current))
        res
      (docblock--merge-get-doc-until-first-tag lines
                                               (+ 1 n)
                                               (concat res current "\n")))))

(defun docblock--merge-docblock-lines (string-old string-new)
  "Merge two docblocks.

STRING-OLD is the existing docblock.  STRING-NEW is the newly
generated."
  (cl-flet ((filter-params (lines)
                           (cl-remove-if-not
                            (lambda (line)
                              (string-match docblock-tag-regexp
                                            line))
                            lines)))
    (let* ((pure-old-lines (split-string string-old "\n" t))
           (pure-new-lines (split-string string-new "\n" t))
           (filtered-old-lines (filter-params pure-old-lines))
           (filtered-new-lines (filter-params pure-new-lines))
           (beginning-block (docblock--merge-get-doc-until-first-tag
                             pure-old-lines))
           (param-block (docblock--merge-recurse 0
                                                 0
                                                 filtered-new-lines
                                                 filtered-old-lines
                                                 pure-new-lines
                                                 pure-old-lines)))
      (concat beginning-block param-block
              (nth (- (length pure-new-lines) 1) pure-new-lines)
              "\n"))))

(defun docblock--normalize-match-string (num &optional no-trim)
  ""
  (let ((string (match-string-no-properties num)))
    (if (null string)
        ""
      (if no-trim
          string
        (string-trim string)))))

;;;###autoload
(define-minor-mode docblock-mode
  "Minor-mode for generating, updating and editing documentation
blocks.

\\{docblock-mode-map}"
  :init-value nil
  :keymap docblock-mode-map
  :lighter " docblock"
  (when docblock-mode
    (docblock--if-major-mode-implementation-existing
      ;; if special mode we don't have to do anything
      nil
      ;; call init function for mode
      (funcall (cdr entry)))))

(provide 'docblock-mode)
;;; docblock-mode.el ends here
