;;; docblock-php-test.el ---                         -*- lexical-binding: t; -*-

;; Copyright (C) 2018  Sebastian Fieber

;; Author: Sebastian Fieber <sebastian.fieber@web.de>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'docblock-mode)
(require 'docblock-php)
(require 'ert)
(require 'php-mode)

(defvar docblock-php-test-file (expand-file-name "test/test.php"))

(defvar docblock-php-function-basic-test-result
  (regexp-quote
   "/**
     *
     *
     * @param mixed $varA
     * @param mixed $varB
     */"))

(defvar docblock-php-function-return-tag-result
  (regexp-quote
   "/**
     *
     *
     * @param mixed $varA
     * @param mixed $varB
     * @return mixed
     */"))

(defvar docblock-php-function-throw-tag-result
  (regexp-quote
   "/**
     *
     *
     * @param mixed $varA
     * @param mixed $varB
     * @throws SomeException
     */"))

(defvar docblock-php-function-return-and-throw-tag-result
  (regexp-quote
   "/**
     *
     *
     * @param mixed $varA
     * @param mixed $varB
     * @return mixed
     * @throws SomeException
     */"))

(defvar docblock-php-function-class-type-result
  (regexp-quote
   "/**
     *
     *
     * @param ClassA $classAVar
     * @param ClassB $classBVar
     * @param mixed $otherVar
     */"))

(defmacro docblock-php-with-default-test-setup (search &rest should)
  "Macro for basic ert tests.

Sets up test environment and enables `php-mode' and jumps to
SEARCH.  SHOULD is used as the body of erts should macro."
  (declare (indent 0))
  `(let ((docblock-use-yasnippet nil))
     (with-temp-buffer
       (insert-file-contents docblock-php-test-file)
       (php-mode)
       (search-forward ,search)
       (docblock-insert)
       (delete-trailing-whitespace)
       (search-backward "/**")
       (should ,@should))))

(ert-deftest docblock-php-function-basic-test ()
  (docblock-php-with-default-test-setup
    "functionA"
    (looking-at docblock-php-function-basic-test-result)))

(ert-deftest docblock-php-function-default-value-test ()
  (docblock-php-with-default-test-setup
    "functionB"
    (looking-at docblock-php-function-basic-test-result)))

(ert-deftest docblock-php-function-throw-tag-test ()
  (docblock-php-with-default-test-setup
    "functionC"
    (looking-at docblock-php-function-throw-tag-result)))

(ert-deftest docblock-php-function-return-tag-test ()
  (docblock-php-with-default-test-setup
    "functionD"
    (looking-at docblock-php-function-return-tag-result)))

(ert-deftest docblock-php-function-return-and-throw-tag-test ()
  (docblock-php-with-default-test-setup
    "functionE"
    (looking-at docblock-php-function-return-and-throw-tag-result)))

(ert-deftest docblock-php-function-class-type-test ()
  (docblock-php-with-default-test-setup
    "functionF"
    (looking-at docblock-php-function-class-type-result)))

(ert-deftest docblock-php-function-abstract-test ()
  (docblock-php-with-default-test-setup
    "abstractMethod"
    (looking-at docblock-php-function-basic-test-result)))

(provide 'docblock-php-test)
;;; docblock-php-test.el ends here
