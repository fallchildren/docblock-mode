<?php

namespace SomeNamespace;

use SomeOtherNamespace\SomeOtherClass;
use SomeOtherNamespace\ClassA;
use SomeOtherNamespace\ClassB;
use SomeOtherNamespace\ClassC;

abstract class SomeClass extends SomeOtherClass implements SomeInterface {

    public function functionA($varA, $varB) {
        // todo
    }

    public function functionB($varA = false, $varB = null) {
        // todo
    }

    private function functionC($varA, $varB) {
        // todo
        throw new SomeException();
    }

    private function functionD($varA, $varB) {
        // todo
        return false;
    }

    private function functionE($varA = false, $varB = null) {
        // todo
        throw new SomeException();
        return false;
    }

    function functionF(ClassA $classAVar, ClassB $classBvar, $otherVar) {
        // todo
    }

    abstract function abstractMethod($varA, $varB);

    /**
     * Some documentation
     *
     * @param bool $varA Some doc
     * @param int $varB Some other doc
     * @param array $varC Some really other doc
     */
    public function existingDoc($varA, $varB, $varC, $varD) {
        // todo
        throw new SomeOtherException();
        return false;
    }

    /**
     * Some documentation
     *
     * @param  bool   $varA  Some doc
     * @param  int    $varB  Some other doc
     * @param  array  $varC  Some really other doc
     */
    public function existingDocWhitespace($varA, $varB, $varC) {
        // todo
    }
}