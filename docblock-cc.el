;;; docblock-cc.el ---                               -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Sebastian Fieber

;; Author: Sebastian Fieber <sebastian.fieber@web.de>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

;;\\(?:<[a-zA-Z0-9&*:_-<>, ]+>\\)

(require 'docblock-mode)

(defconst docblock-cc-function-regexp-base
  (concat
   ;; whitespace
   "^\\([\s\t]*\\)"
   ;; keywords
   "\\(?:extern[\s\t\n]+\\)?"
   "\\(?:explicit[\s\t\n]+\\)?"
   "\\(virtual[\s\t\n]+\\)?"
   "\\(static[\s\t\n]+\\)?"
   "\\(const[\s\t\n]+\\)?"
   ;; return type
   "\\([&*\\:_a-zA-Z0-9<>, ]+[\s\n\t]+\\)?" ;; maybe a constructor so no type
   ;; pointer + whitespace
   "\\([*]*[\s\n\t]+\\)?"
   ;; namespace and function decl
   "[~=<>-\\:_a-zA-Z0-9]+[\s\n\t]*("
   ;; parameter list
   "\\([&*\\:=_\"a-zA-Z0-9&<>()\s\n\t,\.]*?\\))+[\s\n\t]*"
   "\\(?:const[\s\n\t]*\\)?"
   "\\(?:override[\s\n\t]*\\)?"))

(defconst docblock-cc-function-regexp
  (concat
   docblock-cc-function-regexp-base
   "[\s\t\n]*{"))

(defconst docblock-cc-function-no-body-regexp
  (concat
   docblock-cc-function-regexp-base
   ";$"))

(defconst docblock-cc-variable-regexp
   (concat
    "^\\([\s\t]*\\)"
    "\\(static\\)?[\s\n\t]*"
    "\\(const\\)?[\s\n\t]*"
    "\\([:a-zA-Z0-9_]+\\) ?=? ?.*;"))

(defconst docblock-cc-class-regexp
  (concat
   "^\\([\s\t]*\\)"
   "\\(class\\|struct\\)"))

(defconst docblock-cc-strip-regexp
  "\\(\*\\|&\\|const\\|struct\\)")

;;;###autoload
(defun docblock-cc-init ()
  ""
  (interactive)
  (setq docblock-function-regexp         docblock-cc-function-regexp
        docblock-function-decl-regexp    docblock-cc-function-no-body-regexp
        docblock-variable-regexp         docblock-cc-variable-regexp
        docblock-class-regexp            docblock-cc-class-regexp
        docblock-parse-function-function 'docblock-cc-parse-function
        docblock-parse-variable-function 'docblock-cc-parse-variable
        docblock-parse-class-function    'docblock-cc-parse-class
        docblock-tag-whitelist           '("@param"
                                           "@return"
                                           "@throw")))

(defun docblock-cc-parse-class ()
  (let ((space (docblock--normalize-match-string 1)))
    `(,space nil)))

(defun docblock-cc-parse-variable ()
  (save-excursion
    (let* ((full (docblock--normalize-match-string 0))
           (space (docblock--normalize-match-string 1))
           (static (docblock--normalize-match-string 2))
           (const (docblock--normalize-match-string 3))
           (name (docblock--normalize-match-string 4)))
      `(,space nil))))

(defun docblock-cc-parse-function ()
  (save-excursion
    (save-restriction
      (save-match-data
        (let ((bounds (docblock--get-function-bounds)))
          (narrow-to-region (car bounds) (cdr bounds))))
      (save-match-data
        (let* ((full-sig (match-string-no-properties 0))
               (real-raw-params (docblock--normalize-match-string 7))
               (raw-return (docblock--normalize-match-string 5))
               (space (docblock--normalize-match-string 1 t))
               (virtual (docblock--normalize-match-string 2))
               (static (docblock--normalize-match-string 3))
               (const (docblock--normalize-match-string 4))
               (raw-params (string-trim
                            (replace-regexp-in-string "[\n\t\s]+"
                                                      " "
                                                      real-raw-params)))
               (param-list (mapcar
                            (lambda (param)
                              `(,(concat "@param " (cadr
                                                    (split-string
                                                     (replace-regexp-in-string
                                                      docblock-cc-strip-regexp
                                                      ""
                                                      param)
                                                     nil
                                                     t
                                                     "\s")))
                                . t))
                            (split-string raw-params "," t "[\s\t\n]*")))
               (return (if (not (string-match "void$" raw-return))
                           '("@return" .  t)
                         nil))
               (throws (save-match-data
                         (let ((list '()))
                           (while (re-search-forward "throw \\(new \\)?\\([:a-zA-Z0-9_]+\\)" nil t)
                             (cl-pushnew `(,(concat "@throw "
                                                    (match-string-no-properties 2)
                                                    " ${}") . t)
                                         list))
                           list)))
               (tag-list (cl-remove-if #'null
                                       `(,@param-list ,return ,@throws))))
          (list space tag-list))))))

(provide 'docblock-cc)
;;; docblock-cc.el ends here
