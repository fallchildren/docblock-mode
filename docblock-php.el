;;; docblock-php.el ---                              -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Sebastian Fieber

;; Author: Sebastian Fieber <sebastian.fieber@web.de>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'docblock-mode)

(defconst docblock-php-function-regexp
  (concat
   ;; indentation
   ;; info: anchor is necessary for backward searching else the match
   ;; will ignore optional stuff at the beginning
   "^\\([\s\t]*\\)"
   "\\(abstract[\s\n\t]*\\)?"
   ;; access
   "\\(public[\s\n\t]*\\|private[\s\n\t]*\\|protected[\s\n\t]*\\)?"
   ;; scope
   "\\(static[\s\n\t]*\\)?"
   ;; name
   "function[\s\n\t]+\\([_a-zA-Z0-9]*\\)[\s\n\t]*("
   ;; parameters
   "\\([\]\[=_\"a-zA-Z0-9&\s\n\t,$\.]*\\))"))

(defconst docblock-php-function-no-body-regexp
  (concat
   docblock-php-function-regexp
   ";$"))

(defconst docblock-php-variable-regexp
  (concat
   "^\\([\s\t]*\\)"
   "\\(public\\|private\\|protected\\)?[\s\n\t]*"
   "\\(const\\)?[\s\n\t]*"
   "\\($?[a-zA-Z0-9_]+\\) ?=? ?.*;"))

(defconst docblock-php-class-regexp
  (concat
   "^\\([\s\t]*\\)"
   "\\(abstract\\)?[\s\t]*"
   "\\(class\\|trait\\|interface\\)"))

(defvar-local docblock-php-test-regexp "Test$")

(defconst docblock-php-type-list
  '("string"
    "int"
    "float"
    "bool"
    "array"
    "resource"
    "null"
    "callable"
    "object"
    "self"))

(defun docblock-php-prepare-tag-line-regexp (regexp)
  "Function for `docblock-prepare-tag-line-regexp'.

Will replace type specifier mixed with a regular expression matching
any type specifier."
  (if docblock-use-yasnippet
      (replace-regexp-in-string (regexp-quote
                                 (regexp-quote
                                  "${mixed$$(yas-choose-value docblock-php-type-list)}"))
                                "[a-zA-Z0-9]+"
                                regexp)
    (replace-regexp-in-string "mixed" "[a-zA-Z0-9]+" regexp)))

;;;###autoload
(defun docblock-php-init ()
  ""
  (interactive)
  (setq docblock-function-regexp         docblock-php-function-regexp
        docblock-function-decl-regexp    docblock-php-function-no-body-regexp
        docblock-prepare-tag-line-regexp 'docblock-php-prepare-tag-line-regexp
        docblock-parse-function-function 'docblock-php-parse-function
        docblock-parse-variable-function 'docblock-php-parse-variable
        docblock-parse-class-function    'docblock-php-parse-class
        docblock-variable-regexp         docblock-php-variable-regexp
        docblock-class-regexp            docblock-php-class-regexp
        docblock-beginning-of-statement  #'beginning-of-line
        docblock-tag-whitelist           '("@param"
                                           "@return"
                                           "@throws"
                                           "@var"
                                           "@author")))

(defun docblock-php-parse-class ()
  (let ((space (and (not (null (match-string 1)))
                    (docblock--normalize-match-string 1))))
    (list space
          (list`(,(concat "@author " user-full-name " <" user-mail-address ">") . nil)))))

(defun docblock-php-parse-variable ()
  (save-excursion
    (let* ((full (docblock--normalize-match-string 0 t))
           (space (docblock--normalize-match-string 1 t))
           (access (docblock--normalize-match-string 2))
           (scope (docblock--normalize-match-string 3))
           (name (docblock--normalize-match-string 4)))
      (if docblock-use-yasnippet
          (list space
                (list `(,(concat "@var ${mixed$$(yas-choose-value docblock-php-type-list)} " name) . nil)))
        (list space
              (list `(,(concat "@var mixed " name) . nil)))))))

(defun docblock-php-parse-function ()
  "some stuff"
  (save-excursion
    (save-restriction
      (save-match-data
        (let ((bounds (docblock--get-function-bounds)))
          (narrow-to-region (car bounds) (cdr bounds))))
      (save-match-data
        (let* ((name (docblock--normalize-match-string 5))
               (real-raw-params (match-string-no-properties 6))
               (full-sig (docblock--normalize-match-string 0 t))
               (space    (docblock--normalize-match-string 1 t))
               (abstract (docblock--normalize-match-string 2))
               (access   (docblock--normalize-match-string 3))
               (scope (docblock--normalize-match-string 4))
               (raw-params (string-trim
                            (replace-regexp-in-string
                             "[\n\t\s]+"
                             " "
                             real-raw-params)))
               (param-list (mapcar
                            (lambda (param)
                              (let* ((split (split-string param))
                                     (split-filtered (let ((found nil))
                                                       (seq-take-while
                                                        (lambda (val)
                                                          (if found
                                                              nil
                                                            (setq found
                                                                  (string-match-p "^\\$" val))
                                                            t))
                                                        split))))
                                (if (> (length split-filtered) 1)
                                    `(,(concat "@param "
                                               (car split-filtered)
                                               " "
                                               (cadr split-filtered))
                                      . tt)
                                  `(,(concat
                                      (if docblock-use-yasnippet
                                          "@param ${mixed$$(yas-choose-value docblock-php-type-list)} "
                                        "@param mixed ")
                                             (car split-filtered))
                                    . t))))
                            (split-string raw-params "," t "[\s\t\n]*")))
               (return (save-match-data
                         (save-excursion
                           (if (re-search-forward "return " nil t)
                               (if docblock-use-yasnippet
                                   '("@return ${mixed$$(yas-choose-value docblock-php-type-list)}" .  t)
                                 '("@return mixed " . t))
                             nil))))
               (throws (save-match-data
                         (let ((list '()))
                           (while (re-search-forward "throw new \\([\\a-zA-Z0-9_]+\\)" nil t)
                             (cl-pushnew `(,(concat "@throws "
                                                    (docblock--normalize-match-string 1)
                                                    (if docblock-use-yasnippet
                                                        " ${}"
                                                      " ")) . t) list
                                          ;; we don't want duplicates here
                                         :test #'equal))
                           list)))
               (test (when (string-match docblock-php-test-regexp name)
                       '("@test")))
               (tag-list (cl-remove-if #'null
                                       `(,@param-list ,return ,@throws ,test))))
          (list space tag-list))))))


(provide 'docblock-php)
;;; docblock-php.el ends here
